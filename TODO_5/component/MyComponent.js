import React from "react";
import { View, StyleSheet, Text } from "react-native";

const MyComponent = () => {
    const name = 'Kuenley Tshewang Dendup';
    return (
        <View>
            <Text style = {styles.textStyle}>Getting Started with react native!</Text>
            <Text style = {styles.textStyle1}> My name is  {name}</Text>
            
        </View>
    )
};
 

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 45
        },
    textStyle1:{
        fontSize:20
    }
})
export default MyComponent;