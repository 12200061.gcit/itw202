import React from 'react'
import { View, StyleSheet } from 'react-native'
import {DrawerItem, DrawerContentScrollView} from '@react-navigation/drawer';
import { Text, Avatar, Title, Caption, Paragraph, Drawer, TouchableRipple, Switch } from 'react-native-paper';

export default function DrawerContent() {
  return (
<DrawerContentScrollView>
    <View style={Styles.drawerContent}>
        <View style={Styles.userInfoSection}>
            <Avatar.Image 
                size = {80} 
                source={require("../../assets/profile.jpg")}
                />
            <Title style= {Styles.title}>Tshewang Dendup</Title>
            <Caption style={Styles.caption}>@12200061.gcit</Caption> 
            <View style = {Styles.row}>
                <View style={Styles.section}>
                    <Paragraph style = {[Styles.paragraph, Styles.caption]}>
                        199
                        </Paragraph>
                        <Caption style={Styles.caption}>Following</Caption>
                            </View>
                            <View style={Styles.section}>
                                <Paragraph style={[Styles.paragraph, Styles.caption]}>
                                    199
                                </Paragraph>
                                <Caption style={Styles.caption}>Followers</Caption>
                                
                            </View>
                        </View>
                    </View>
                    <Drawer.Section style={Styles.drawerSection}>
                        <DrawerItem label='Setting' onPress={()=>{}}/>
                    </Drawer.Section>

                    <Drawer.Section  title='Preferences'>
                        <TouchableRipple onPress={()=>{}}>
                            <View style={Styles.preferences}>
                                <Text>Notifications</Text>
                                <View pointerEvents='none'>
                                    <Switch value={false}/>
                                </View>
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    </View>
                
</DrawerContentScrollView>  )
}

const Styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
        
    },
    userInfoSection:{
        paddingLeft: 20,
    },
    title:{
        marginTop: 20,
        fontWeight: 'bold'
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around',
        margin:15
    },
    section:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        marginRight:15
    },
    paragraph:{
        fontWeight:'bold',
        marginRight:7

    },
    preferences:{
        flex:1, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        margin:20,
        color:'black'
    },
    lower:{
        borderRadius:40
    }

});

