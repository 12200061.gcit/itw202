import React from 'react'
import Background from '../components/Background'
import { ActivityIndicator } from 'react-native'
import firebase from 'firebase/app'

export default function AuthLoadingScreen({navigation}) {
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            navigation.reset({
                routes: [{ name: "HomeScreen"}],
            });
        } else {
            navigation.reset({
                routes: [{name: "StartScreen"}],
            });
        }
    });
  return (
    <Background>
        <ActivityIndicator size = "large" />
    </Background>
  )
}
