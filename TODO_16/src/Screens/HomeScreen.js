import React from 'react'
import { logoutUser } from '../api/auth-api'
import Background from '../components/Background'
import Button from '../components/Button'
import Header from '../components/Header'

export default function HomeScreen() {
  return (
    <Background>
        <Header>Home</Header>
        <Button mode ="contained"
        onPress={() => { logoutUser();}}
        >Logout</Button>
    </Background>
  )
}
