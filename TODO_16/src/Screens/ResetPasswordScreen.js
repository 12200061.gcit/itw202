import React, {useState} from "react";
import { TouchableOpacity, StyleSheet, View, Text } from "react-native";
import Button from "../components/Button";
import Header from "../components/Header";
import Background from "../components/Background";
import BackButton from "../components/BackButton";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import { theme } from "../core/theme";
import { resetPassword } from "../api/auth-api";

export default function ResetPasswordScreen({navigation}){
    const [email,setEmail] = useState({value:"", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value); 
       
        if(emailError){
            setEmail({ ...email,error:emailError });
        }
        setLoading(true)
        const response = await resetPassword({
            email: email.value
        });
        if (response.error) {
            alert(response.error);
        }
        else{
           alert("Password reset link sent!")
        }
        setLoading(false)
    }


    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({value:text,error:""})}
                description="You will receive email with password reset link."
            />
            <Button loading = {loading} mode = "contained" onPress={onSubmitPressed}>Send Instructions</Button>
        
        </Background>
    
    )
}
