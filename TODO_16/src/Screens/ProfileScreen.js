import React from 'react'
import Background from '../components/Background'
import Header from '../components/Header'

export default function ProfileScreen() {
  return (
    <Background>
        <Header>Profile</Header>
    </Background>
  )
}
