import React, {useState} from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import BackButton from '../components/BackButton'
import Background from '../components/Background'
import Button from '../components/Button'
import Header from '../components/Header'
import Logo from '../components/Logo'
import Paragraph from '../components/Paragraph'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helpers/emailValidators'
import { passwordValidator } from '../core/helpers/passwordValidator'
import { theme } from '../theme'


export default function LoginScreen({navigation}) {
    const [email, setEmail] = useState({value: "", error: ""})
    const [password, setPassword] = useState({value: "", error: ""})

    const onLoginPressed = () => {
      const emailError = emailValidator(email.value);
      const passwordError = passwordValidator(password.value);
      if (emailError || passwordError) {
        setEmail({...email, error: emailError});
        setPassword({...password, error: passwordError});
      }
      else{
        navigation.navigate('HomeScreen')
      }
    }
  return (
    <Background> 
      <BackButton goBack = {navigation.goBack} />
        <Logo />
        <Header> Welcome</Header>
        <TextInput label="Email"
        value={email.value}
        error={email.error}
        onChangeText={(text)=> setEmail({value: text, error:""})}/>
        <TextInput 
        label = "Password"
        value={password.value}
        error={password.error}
        onChangeText={(text)=> setPassword({value: text, error:""})}
        secureTextEntry/>
        <View style = {styles.forgotPassword}>
        <TouchableOpacity onPress={()=> navigation.navigate("ResetPasswordScreen")}>
          <Text style = {styles.forgot}>Forgot your password?</Text>
          </TouchableOpacity>
        </View>
        <Button mode="contained" onPress = {onLoginPressed}>Login</Button>
        <View style = {styles.row}>
          <Text>Don't have an account?</Text>
        <TouchableOpacity onPress={()=> navigation.replace("RegisterScreen")}>
          <Text style = {styles.link}> SignUp</Text>
        </TouchableOpacity>
        </View>
    </Background>
  )
}
const styles = StyleSheet.create({
  row:{
    flexDirection: 'row',
    marginTop: 4,
  },
  link:{
    fontWeight: 'bold',
    color:theme.colors.primary,
  },
  forgot:{
    fontSize: 13,
    color: theme.colors.secondary
  },
  forgotPassword:{
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  }
})
