import React from 'react'
import Background from '../components/Background'
import Header from '../components/Header'

export default function HomeScreen() {
  return (
    <Background>
        <Header>Home</Header>
    </Background>
  )
}
