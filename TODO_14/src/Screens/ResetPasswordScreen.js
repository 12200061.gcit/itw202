import React, {useState} from 'react'
import { View, StyleSheet } from 'react-native'
import BackButton from '../components/BackButton'
import Background from '../components/Background'
import Button from '../components/Button'
import Header from '../components/Header'
import Logo from '../components/Logo'
import Paragraph from '../components/Paragraph'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helpers/emailValidators'
import { passwordValidator } from '../core/helpers/passwordValidator'
import { theme } from '../theme'

export default function ResetPasswordScreen({navigation}) {
    const [email, setEmail] = useState({value: "", error: ""})
    
    const onSubmitPressed = () => {
      const emailError = emailValidator(email.value);
      
      if (emailError) {
        setEmail({...email, error: emailError});
        }
    }
  return (
    <Background> 
      <BackButton goBack = {navigation.goBack} />
        <Logo />
        <Header> Restore Password</Header>
        <TextInput label="Email"
        value={email.value}
        error={email.error}
        onChangeText={(text)=> setEmail({value: text, error:""})}
        description="You will recieve email with password reset link"
        />
        
        <Button mode="contained" onPress = {onSubmitPressed}>Send Instructions</Button>
       
    </Background>
  )
}
const styles = StyleSheet.create({
  row:{
    flexDirection: 'row',
    marginTop: 4,
  },
  link:{
    fontWeight: 'bold',
    color:theme.colors.primary,
  },
})
