import { StatusBar } from 'expo-status-bar';
import { NativeAppEventEmitter, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import { StartScreen, LoginScreen, RegisterScreen, ResetPasswordScreen,HomeScreen, ProfileScreen } from './src/Screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Image } from 'react-native';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName='StartScreen'
      screenOptions = {{headerShown: false}}>
        
        <Stack.Screen name='StartScreen' component={StartScreen} />
        <Stack.Screen name='LoginScreen' component={LoginScreen} />
        <Stack.Screen name='RegisterScreen' component={RegisterScreen} />
        <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen} />
        <Stack.Screen name='HomeScreen' component={BottomNavigation} />
      </Stack.Navigator>
      <Drawer.Navigator useLegacyImplementation initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
      <StatusBar style="auto" />
   
    </Provider>
  );
}

function BottomNavigation(){
  return(
    <Tab.Navigator>
      <Tab.Screen name = "Home" component={HomeScreen} 
      options={{
        tabBarIcon: ({size}) => {
          return(
            <Image
            style={{width: size, height: size}}
            source={
              require('./assets/home-icon.png')
            } />
          )
        }
      }}/>
      <Tab.Screen name = "Profile" component={ProfileScreen}
       options={{
         tabBarIcon: ({size}) => {
           return(
             <Image
             style={{width: size, height: size}}
             source={
               require('./assets/setting-icon.png')
             } />
           )
         }
       }}/>

    </Tab.Navigator>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
