import { LinearGradient } from 'expo-linear-gradient';
import { StatusBar } from 'expo-status-bar';
import { Image, ImageBackground, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import StartGameScreen from './Screen/StartGameScreen';
import { useState } from 'react';
import GameScreen from './Screen/GameScreen';
import Colors from './constants/Colors';
import GameOverScreen from './Screen/GameOverScreen';


export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRounds, setGuessRounds] = useState(0)


  function pickedNumberHandler(pickerNumber) {
    setUserNumber(pickerNumber)
    setGameIsOver(false);
  }

  function gameOverHandler(numberOfRounds) {
    setGameIsOver(true);
    setGuessRounds(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
    if (userNumber){
      screen = <GameScreen number = {userNumber} onGameOver = {gameOverHandler}/>
    }
    if (gameIsOver && userNumber) {
      screen = <GameOverScreen 
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
        />
      
    }

  return (
    <>
    <StatusBar style= 'light'/>
    <LinearGradient 
      style={styles.container} 
      colors={[Colors.primary700,Colors.accent500]}>
        <ImageBackground 
        source={require('./assets/dice.jpg')}
        resizeMode="cover"
        style={styles.rootScreen}
        imageStyle={styles.backgroundImage} >
      <SafeAreaView style={styles.container}>
        {screen}
        </SafeAreaView>
        </ImageBackground>
    </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,   
  },
  rootScreen:{
    flex: 1,
    },
    backgroundImage:{
      opacity:0.15
    }
});
