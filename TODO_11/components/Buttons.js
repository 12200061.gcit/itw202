import React, {useState} from 'react';
import { View, Text, StyleSheet, Touchable, Button } from 'react-native';
const AppButton= () => {
    const [ count, setCount ] = useState(0);
    const [isDisabled, setIsDisabled] = useState(false);

    const buttonDisable = () => {
        if (count == 3) {
              setIsDisabled(true)  
        }
        else{
            setCount(count +1) 
        }
        
    }

    return (
        <View>
            <Text>
                {count > 0
                ? 'The Button was pressed ${count} times!'
                : 'The button isnt pressed yet' }
            </Text>
            <Button onPress={() =>buttonDisable()}
            title="Click Me!" disabled={isDisabled} />
            
        </View>
    );
};

export default AppButton;