import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './components/funcCompnent'
export default function App() {
  return (
    <View style={styles.container}>
      <Text>Practical 2</Text>
      <Text>Stateless anf Statefull components</Text>
      <Text style ={styles.text}>
        You are ready to start the journey.
      </Text>
  <StatusBar style="auto" />
  <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    marginTop: '5%',
  }
});
