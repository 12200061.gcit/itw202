import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style = {styles.container}>
      <View style = {styles.box1}><Text style ={styles.text}>1</Text></View>  
      <View style = {styles.box2}><Text style ={styles.text}>2</Text></View>
      <View style = {styles.box3}><Text style ={styles.text}>3</Text></View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    marginTop: 30,
    justifyContent: 'flex-start',
    marginLeft: 30

   },
   box1: {
    height:250,
    width:65,
    backgroundColor: "red",
  },
  box2: {
    height:250,
    width:175,
    backgroundColor: "blue",
  },
  box3: {
    height:250,
    width:25,
    backgroundColor: "green",
  },
  text: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:'center',
    paddingTop: 125
  },
});
