import React from 'react'
import { View,Image, StyleSheet } from 'react-native'

export default function ImageComponent() {
  return (
   <View>
       <Image
       source={require('../assets/favicon.png')} style = {{width: 200,
        height: 100,
        resizeMode: 'contain'}}/>
       <Image
       style={StyleSheet.logoStretch}
       source={{uri: 'https://about.gitlab.com/images/topics/devops-lifecycle.png'}} style={{width: 200, height: 100, resizeMode: 'stretch'}}/>
   </View>
  )
}

const styles = StyleSheet.create({
    logoContain: {
        width: 200,
        height: 100,
        resizeMode: 'contain',
    },
    logoStretch: {
        width :200, height: 100,
        resizeMode: 'stretch',
    }
})
