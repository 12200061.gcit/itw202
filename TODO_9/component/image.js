import React from 'react'
import { View,Image, StyleSheet } from 'react-native'

export default function ImageComponent() {
  return (
   <View>
       <Image
       source={require('../assets/logo.png')} style = {{width: 100,
        height: 100,
        }}/>
       <Image
        source={{uri: 'https://picsum.photos/100/100'}} style={{width: 100, height: 100}}/>
   </View>
  )
}