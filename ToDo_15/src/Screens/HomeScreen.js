import { View, Text, Button, Image} from 'react-native'
import React from 'react'
import Header from '../components/Header'
import Background from '../components/Background'

export default function HomeScreen() {
  return (
    <Background>
        <Header> Hi! Welcome to Home Screen.</Header>
        <Image style={{width:100, height:100, borderRadius:20}}
        source={require('../../assets/home.png')}
        />
    </Background>
  )
}