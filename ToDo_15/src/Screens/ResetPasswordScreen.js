import React, {useState}from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Header from '../components/Header';
import CustomInput from '../components/CustomInput';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';
import { emailValidator } from '../core/Helpers/emailvalid';
import GoBack from '../components/BackBut';
import { theme } from '../core/theme';

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail]=useState({value:"", error:""})
    const [loading, setLoading]=useState();
    const onResetPress=()=>{
        const emailError = emailValidator(email.value);
        if(emailError||passwordError){
            setEmail({...email, error: emailError});
        }
    }
    return (
        <Background>
            <GoBack goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <CustomInput 
                label='Email'
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=> setEmail({value: text, error:""})}
                description="You will receive email with password reset link."
            />
            <Button mode="contained" onPress={onResetPress}>Send Instructions</Button>
        </Background>
        
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%'
      
    },
    row:{
        flexDirection:'row',
        marginTop:4
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    }
  });