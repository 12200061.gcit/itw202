import React, {useState}from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Header from '../components/Header';
import CustomInput from '../components/CustomInput';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';
import { emailValidator } from '../core/Helpers/emailvalid';
import { passwordValidator } from '../core/Helpers/passwordvalid';
import GoBack from '../components/BackBut';
import { theme } from '../core/theme';
import { loginUser } from '../api/auth-api';


export default function LoginScreen({navigation}){
    const [email, setEmail]=useState({value:"", error:""})
    const [password, setPassword]=useState({value:"", error:""})
    const [loading, setLoading] = useState();

    const onLoginPress= async()=>{
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        if(emailError||passwordError){
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
        }
        setLoading(true)
        const response = await loginUser({
            email: email.value,
            password: password.value,
        });
        if (response.error) {
            alert(response.error);
        } else {
            alert(response.user.displayName);
        }
        setLoading(false)
        
    }
    return (
        <Background>
            <GoBack goBack={navigation.goBack}/>
            <Logo/>
            <Header>Welcome</Header>
            <CustomInput 
                label='Email'
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=> setEmail({value: text, error:""})}
            />
            <CustomInput 
                secureTextEntry
                label='Password'
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text1)=> setPassword({value: text1, error:""})}
            />
            <View style={styles.forgotPassword}>
                <TouchableOpacity 
                    onPress={()=>navigation.navigate("ResetPasswordScreen")}>
                    <Text style={styles.forgot}> Forgot Your password? </Text>
                </TouchableOpacity>
            </View>
            <Button loading = {loading} mode="contained" onPress={onLoginPress}>Login</Button>
            <View style={styles.row}>
                <Text>Don't have an account? </Text>
                <TouchableOpacity onPress={()=>navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>  Sign up</Text>
                </TouchableOpacity>
            </View>
        </Background>
        
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%'
      
    },
    row:{
        flexDirection:'row',
        marginTop:4
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    }
  });