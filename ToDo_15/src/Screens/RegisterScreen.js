import React, {useState}from 'react';
import {View, StyleSheet,Text} from 'react-native';
import Header from '../components/Header';
import CustomInput from '../components/CustomInput';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';
import { emailValidator } from '../core/Helpers/emailvalid';
import { passwordValidator } from '../core/Helpers/passwordvalid';
import { nameValidator } from '../core/Helpers/namevalid';
import GoBack from '../components/BackBut';
import { TouchableOpacity } from 'react-native';
import { theme } from '../core/theme';
import { signUpUser } from '../api/auth-api';


export default function RegisterScreen({navigation}){
    
    const [email, setEmail]=useState({value:"", error:""})
    const [password, setPassword]=useState({value:"", error:""})
    const [name, setName]=useState({value:"", error:""})
    const [loading, setLoading] = useState();

    const onSignupPress= async()=>{
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        const nameError = nameValidator(name.value);
        if(emailError||passwordError||nameError){
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
            setName({...name, error: nameError});
        }
        setLoading(true)
        const response = await signUpUser({
            name: name.value,
            email:email.value,
            password: password.value
        })
        if (response.error){
            alert(response.error)
        }
        else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }
        setLoading(false)
    }

    return (
        <Background>
            <GoBack goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create Account</Header>
            <CustomInput 
                label='Name'
                value={name.value}
                error={name.error}
                errorText={name.error}
                onChangeText={(text)=> setName({value: text, error:""})}
            />
            <CustomInput 
                label='Email'
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=> setEmail({value: text, error:""})}
            />
            <CustomInput 
                secureTextEntry
                label='Password'
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text)=> setPassword({value: text, error:""})}
            />
            <Button loading= {loading} mode="contained" onPress={onSignupPress}>Sign Up</Button>
            <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity onPress={()=>navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>  Login</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%'
      
    },
    row:{
        flexDirection:'row',
        marginTop:4
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    }

  });