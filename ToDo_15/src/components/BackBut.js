import React from "react"
import { StyleSheet, TouchableOpacity, Image } from "react-native"
import { getStatusBarHeight } from "react-native-status-bar-height"

export default function GoBack({ goBack }){
    return(
        <TouchableOpacity
            onPress={goBack} style={styles.container}>
            <Image 
                style={styles.image}
                source={require('../../assets/BackButton.png')}
            />
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:10 + getStatusBarHeight(),
        left:4,
    },

    image:{
        width:24,
        height:24,
    }
})
