import { View, Text } from 'react-native'
import React from 'react'
import {StartScreen,LoginScreen,RegisterScreen,ResetPasswordScreen, HomeScreen } from '../src/Screens/Index';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import 'react-native-gesture-handler'
import MyDrawer from './MyDrawer'

const Stack = createNativeStackNavigator(); 

export default function Mystack() {
  return (
    <Stack.Navigator initialRouteName='StartScreen' screenOptions={{headerShown:false}} >
      <Stack.Screen name='StartScreen' component={StartScreen}/>
      <Stack.Screen name='LoginScreen' component={LoginScreen}/>
      <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
      <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
      <Stack.Screen name='HomeScreen' component={MyDrawer}/>
    </Stack.Navigator>  
  )
}