import { View,StyleSheet } from 'react-native'
import React from 'react'
import {DrawerItem, DrawerContentScrollView} from '@react-navigation/drawer'
import {Avatar, Title, Caption, Paragraph,Drawer, Text, TouchableRipple, Switch } from 'react-native-paper'
import ProfileScreen from '../src/Screens/ProfileScreen';
import MyTab from './MyTab'

function DrawerContent(props){
    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={[styles.header]}>
                        <View style={{margin:15,flex:1, alignItems:'center'}}>
                            <Avatar.Image 
                            size={80}
                            source={require('../assets/profile.jpg')}/> 
                        </View>
                        <View style={{flex:1, alignItems:'center'}}>
                            <Title style={styles.title}>Tshewang Dendup</Title>
                            <Caption style={styles.caption}> @todo_15-gcit </Caption>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>
                                    54
                                </Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>
                                    199
                                </Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                        </View>
                    </View>

                    <View style={styles.lower}>
                    <Drawer.Section style={styles.dd}>
                        <DrawerItem label='Setting' onPress={()=>{}}/>
                    </Drawer.Section>

                    <Drawer.Section  title='Preferences'>
                        <TouchableRipple onPress={()=>{}}>
                            <View style={styles.preferences}>
                                <Text>Notifications</Text>
                                <View pointerEvents='none'>
                                    <Switch value={false}/>
                                </View>
                            </View>
                        </TouchableRipple>
                    </Drawer.Section>
                    </View>
                </View>
            </DrawerContentScrollView>   
        </View>       
    )
}

const styles = StyleSheet.create({
    drawerContent:{
        flex:1
    },
    header:{
        backgroundColor:'pink',
    },
    title:{
        fontWeight:'bold'
    },
    caption:{
        fontSize:14,
        lineHeight:14,
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around',
        margin:15
    },
    section:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        marginRight:15
    },
    paragraph:{
        fontWeight:'bold',
        marginRight:7

    },
    preferences:{
        flex:1, 
        flexDirection:'row', 
        justifyContent:'space-between', 
        margin:20,
        color:'black'
    },
    lower:{
        borderRadius:40
    }

});

export default DrawerContent;