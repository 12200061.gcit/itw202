import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyB-goNACFCvBtluzTe2mw8Fd7m-4fSTn7I",
  authDomain: "todo-84538.firebaseapp.com",
  databaseURL: "https://todo-84538-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "todo-84538",
  storageBucket: "todo-84538.appspot.com",
  messagingSenderId: "468884816636",
  appId: "1:468884816636:web:982c944448ebe6b4658d79"
}
firebase.initializeApp(CONFIG)

export default firebase;
