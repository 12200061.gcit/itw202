import React, {useState} from 'react';
import { StyleSheet, View, TextInput, Button, Modal } from 'react-native';
const Aspirationinput = (props) => {
    const [enteredAspiration, SetEnteredAspiration] = useState('');
    
    const clearInput = (enteredText) => {
        SetEnteredAspiration();
    }


    const AspirationInputHandler = (enteredText) => {
        SetEnteredAspiration(enteredText);
    };

    return (
    
        <Modal visible={props.visible} animationType='slide'>
        <View style={styles.inputContainer}>
            <TextInput
            placeholder='My aspiration from this module'
            style={styles.input}
            onChangeText={AspirationInputHandler}
            value={enteredAspiration} />
            </View>
            <View style={styles.btn}>
            <Button
            title='ADD'
            onPress={() => props.onAddAspiration(enteredAspiration)} style={{justifyContent: 'space-between',alignItems: 'center',}} />
            <Button title="CANCEL" onPress={clearInput}  style={styles.btn}/>
        </View>
        </Modal>
        
 );
}

export default Aspirationinput;
const styles = StyleSheet.create({
    inputContainer:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
      },
      input:{
        marginTop:30,
        width:'80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10
      },
      btn:{
          justifyContent: 'center',
          alignItems:'center',
          flexDirection: 'row',
          padding: 10
      }
     
})