import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    // <>
    // <View style={styles.style1} />
    // <View style={styles.style2} />
    // </>
    
    <View style={style.container} >
      <View style={style.square} />
      <View style={style.square} />
      <View style={style.square} />
  
    </View>
  );
}

// const styles = StyleSheet.create({
//   style1: {
//     flex: 1,
//     backgroundColor: 'lightgoldenrodyellow',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   style2: {
//     flex: 3,
//     backgroundColor: '#7CA1B4',
//     alignItems: 'center',
//     justifyContent:'center'
//   }
const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: "#7CA1B4",
    
  },
  square: {
    backgroundColor: "#7cb48f",
    width: 100,
    height: 100,
    margin: 4,
  },
});
