import React, {useState} from 'react';
import {Text, TextInput, View } from 'react-native';

const TextInputComponent= () => {
    const [text, setText] =useState('');
     return (
        <View style ={{padding: 50, backgroundColor: text}}>
            <Text style ={{marginBottom:10}}>What is your name?</Text>
            <TextInput placeholder=' Enter your name here'    
            multiline={false}
            value = {text}
            onChangeText={newValue => setText(newValue)} 
            onSubmitEditing={newValue => setText(newValue)} />
            <Text style = {{margin: 10}}> Hi {text} from Gyalpozhing College of Information Technology</Text>
           </View>
    );
    
    
}
export default TextInputComponent